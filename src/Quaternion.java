package src;

import static org.junit.Assert.assertEquals;

import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

	private double[] q;
	public static final double DELTA = 0.000001;
	// TODO!!! Your fields here!

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {
		q = new double[] { a, b, c, d };

		// TODO!!! Your constructor here!
	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @return real part
	 */
	public double getRpart() {
		return q[0]; // TODO!!!
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @return imaginary part i
	 */
	public double getIpart() {
		return q[1]; // TODO!!!
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @return imaginary part j
	 */
	public double getJpart() {
		return q[2]; // TODO!!!
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @return imaginary part k
	 */
	public double getKpart() {
		return q[3]; // TODO!!!
	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(getRpart());
		if (getIpart() >= 0)
			sb.append("+");
		sb.append(getIpart());
		sb.append("i");
		if (getJpart() >= 0)
			sb.append("+");
		sb.append(getJpart());
		sb.append("j");
		if (getKpart() >= 0)
			sb.append("+");
		sb.append(getKpart());
		sb.append("k");
		return sb.toString(); // TODO!!!
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */

	public static Quaternion valueOf(String s) {
		String lubatud = "1234567890.+-ijk";
		for (int j = 0; j < s.length(); j++) {
			if (!lubatud.contains(Character.toString(s.charAt(j))))
				throw new RuntimeException("Sisendis on ebasobivad s�mbolid " + s);
		}
		StringTokenizer st = new StringTokenizer(s, "+ijk");
		int a = 0;
		String[] fields = new String[4];
		while (st.hasMoreTokens()) {
			if (a > 3)
				throw new RuntimeException("Sisendis on liiga palju argumente " + s);
			fields[a] = st.nextToken();
			String[] tmp;
			if (fields[0].substring(1).contains(Character.toString('-'))) {
				if (!fields[0].startsWith("-")) {
					tmp = fields[0].split("-");
					fields[0] = tmp[0];
					fields[1] = "-" + tmp[1];
					a = 1;
				} else {
					tmp = fields[0].substring(1).split("-");
					fields[0] = "-" + tmp[0];
					fields[1] = "-" + tmp[1];
					a = 1;
				}
			}
			a++;
		}

		double r = Double.parseDouble((String) fields[0]);
		double i = Double.parseDouble((String) fields[1]);
		double j = Double.parseDouble((String) fields[2]);
		double k = Double.parseDouble((String) fields[3]);
		Quaternion q = new Quaternion(r, i, j, k);
		return q;
	}

	/**
	 * Clone of the quaternion.
	 * 
	 * @return independent clone of <code>this</code>
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Quaternion(getRpart(), getIpart(), getJpart(), getKpart()); // TODO!!!
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {
		if (getRpart() <= 0 - DELTA || getRpart() >= 0 + DELTA)
			return false;
		if (getIpart() <= 0 - DELTA || getIpart() >= 0 + DELTA)
			return false;
		if (getJpart() <= 0 - DELTA || getJpart() >= 0 + DELTA)
			return false;
		if (getKpart() <= 0 - DELTA || getKpart() >= 0 + DELTA)
			return false;
		return true;
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {

		return new Quaternion(getRpart(), -getIpart(), -getJpart(), -getKpart()); // TODO!!!
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		return new Quaternion(-getRpart(), -getIpart(), -getJpart(), -getKpart()); // TODO!!!
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 */
	public Quaternion plus(Quaternion q) {
		return new Quaternion(this.getRpart() + q.getRpart(), this.getIpart() + q.getIpart(),
				this.getJpart() + q.getJpart(), this.getKpart() + q.getKpart()); // TODO!!!
	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 */
	public Quaternion times(Quaternion q) {
		return new Quaternion(
				this.getRpart() * q.getRpart() - this.getIpart() * q.getIpart() - this.getJpart() * q.getJpart()
						- this.getKpart() * q.getKpart(),
				this.getRpart() * q.getIpart() + this.getIpart() * q.getRpart() + this.getJpart() * q.getKpart()
						- this.getKpart() * q.getJpart(),
				this.getRpart() * q.getJpart() - this.getIpart() * q.getKpart() + this.getJpart() * q.getRpart()
						+ this.getKpart() * q.getIpart(),
				this.getRpart() * q.getKpart() + this.getIpart() * q.getJpart() - this.getJpart() * q.getIpart()
						+ this.getKpart() * q.getRpart()); // TODO!!!
	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 */
	public Quaternion times(double r) {
		return new Quaternion(getRpart() * r, getIpart() * r, getJpart() * r, getKpart() * r); // TODO!!!
	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		if (this.isZero())
			throw new RuntimeException(" Nulliga jagamine");
		double nim = getRpart() * getRpart() + getIpart() * getIpart() + getJpart() * getJpart()
				+ getKpart() * getKpart();
		return new Quaternion(getRpart() / nim, -getIpart() / nim, -getJpart() / nim, -getKpart() / nim); // TODO!!!
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 */
	public Quaternion minus(Quaternion q) {
		return new Quaternion(this.getRpart() - q.getRpart(), this.getIpart() - q.getIpart(),
				this.getJpart() - q.getJpart(), this.getKpart() - q.getKpart()); // TODO!!!
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 */
	public Quaternion divideByRight(Quaternion q) {
		if (q.isZero())
			throw new RuntimeException("Nulliga jagamine" + q.toString());
		return this.times(q.inverse()); // TODO!!!
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 */
	public Quaternion divideByLeft(Quaternion q) {
		if (q.isZero())
			throw new RuntimeException("Nulliga jagamine" + q.toString());
		Quaternion invq = q.inverse();
		return invq.times(this);

		// TODO!!!
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 */
	@Override
	public boolean equals(Object qo) {
		if (Math.abs(this.getRpart() - ((Quaternion) qo).getRpart()) > DELTA)
			return false;
		if (Math.abs(this.getIpart() - ((Quaternion) qo).getIpart()) > DELTA)
			return false;
		if (Math.abs(this.getJpart() - ((Quaternion) qo).getJpart()) > DELTA)
			return false;
		if (Math.abs(this.getKpart() - ((Quaternion) qo).getKpart()) > DELTA)
			return false;
		else
			return true;
		// TODO!!!
	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		Quaternion p1 = this.times(q.conjugate());
		Quaternion q1 = q.times(this.conjugate());
		Quaternion sum = p1.plus(q1);
		return new Quaternion(sum.getRpart() / 2, sum.getIpart() / 2, sum.getJpart() / 2, sum.getKpart() / 2); // TODO!!!
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * 
	 * @return hashcode vihjeid lahenduseks sain siit
	 *         https://java.net/nonav/projects/jme/sources/svn/content/tags/v10/
	 *         src/com/jme/math/Quaternion.java?rev=3910 ja siit
	 *         http://worldwind31.arc.nasa.gov/svn/trunk/WorldWind/src/gov/nasa/
	 *         worldwind/geom/Quaternion.java
	 * 
	 */
	@Override
	public int hashCode() {
		int hash = 32;
		hash = (int) (31 * hash + Double.doubleToLongBits(getRpart()) ^ (Double.doubleToLongBits(getRpart()) >>> 32));
		hash = (int) (31 * hash + Double.doubleToLongBits(getIpart()) ^ (Double.doubleToLongBits(getIpart()) >>> 32));
		hash = (int) (31 * hash + Double.doubleToLongBits(getJpart()) ^ (Double.doubleToLongBits(getJpart()) >>> 32));
		hash = (int) (31 * hash + Double.doubleToLongBits(getKpart()) ^ (Double.doubleToLongBits(getKpart()) >>> 32));
		return hash;
		// TODO!!!
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		return Math.sqrt(
				getRpart() * getRpart() + getIpart() * getIpart() + getJpart() * getJpart() + getKpart() * getKpart()); // TODO!!!
	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("first: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
			res = (Quaternion) arv1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println("clone equals to original: " + res.equals(arv1));
		System.out.println("clone is not the same object: " + (res != arv1));
		System.out.println("hashCode: " + res.hashCode());
		res = valueOf(arv1.toString());
		System.out.println("string conversion equals to original: " + res.equals(arv1));
		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[1]);
		System.out.println("second: " + arv2.toString());
		System.out.println("hashCode: " + arv2.hashCode());
		System.out.println("equals: " + arv1.equals(arv2));
		res = arv1.plus(arv2);
		System.out.println("plus: " + res);
		System.out.println("times: " + arv1.times(arv2));
		System.out.println("minus: " + arv1.minus(arv2));
		double mm = arv1.norm();
		System.out.println("norm: " + mm);
		System.out.println("inverse: " + arv1.inverse());
		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
		System.out.println("dotMult: " + arv1.dotMult(arv2));
		Quaternion f = new Quaternion(3., -2., 1., 3.);
		System.out.println("valueOf must read back what toString outputs. " + Quaternion.valueOf(f.toString()));
		System.out.println("valueOf must read back what toString outputs. " + Quaternion.valueOf("-5+2.00i-2.6j+7"));

	}
}
// end of file
